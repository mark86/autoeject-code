const int stepPin = 3;
const int dirPin = 4;
const int sleepPin = 2;

#define forwardPin 8
#define backwardPin 9
#define endstopPin 10

int forward = 0;
int backward = 0;
int endstop_old_value = 0;
int endstop_new_value = 0;


void setup() {
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  pinMode(sleepPin, OUTPUT);
  pinMode(forwardPin, INPUT);
  pinMode(backwardPin, INPUT);
  pinMode(endstopPin, INPUT);


  digitalWrite(sleepPin, LOW);

  Serial.begin(9600);
  Serial.println("Setup");
}
void loop() {

  forward = digitalRead(forwardPin);
  backward = digitalRead(backwardPin);
  endstop_new_value = digitalRead(endstopPin);

  if (endstop_old_value == HIGH && endstop_new_value == LOW) {
    Serial.println("ENDSTOP");
    digitalWrite(dirPin, HIGH); // Enables the motor to move in a particular direction
    digitalWrite(sleepPin, HIGH); 
    for (int y = 0; y < 4; y++) {
      for (int x = 0; x < 10000; x++) {
        digitalWrite(stepPin, HIGH);
        delayMicroseconds(500);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(500);
      }
      Serial.println(y);
    }
    for (int x = 0; x < 6000; x++) {
      digitalWrite(stepPin, HIGH);
      delayMicroseconds(500);
      digitalWrite(stepPin, LOW);
      delayMicroseconds(500);
    }


    delay(3000);

    digitalWrite(dirPin, LOW); // Enables the motor to move in a particular direction

    for (int y = 0; y < 4; y++) {
      for (int x = 0; x < 10000; x++) {
        digitalWrite(stepPin, HIGH);
        delayMicroseconds(500);
        digitalWrite(stepPin, LOW);
        delayMicroseconds(500);
      }
      Serial.println(y);
    }
    for (int x = 0; x < 6000; x++) {
      digitalWrite(stepPin, HIGH);
      delayMicroseconds(500);
      digitalWrite(stepPin, LOW);
      delayMicroseconds(500);
    }

    digitalWrite(sleepPin, LOW);

  }

  endstop_old_value = endstop_new_value;




  if (forward == HIGH) {

    Serial.println("Forward");

    digitalWrite(sleepPin, HIGH);
    digitalWrite(dirPin, HIGH); // Enables the motor to move in a particular directio
    for (int x = 0; x < 100; x++) {
      digitalWrite(stepPin, HIGH);
      delayMicroseconds(500);
      digitalWrite(stepPin, LOW);
      delayMicroseconds(500);
    }

    digitalWrite(sleepPin, LOW);
  }

  if (backward == HIGH) {

    Serial.println("Backward");

    digitalWrite(sleepPin, HIGH);
    digitalWrite(dirPin, LOW); //Changes the rotations direction
    for (int x = 0; x < 100; x++) {
      digitalWrite(stepPin, HIGH);
      delayMicroseconds(500);
      digitalWrite(stepPin, LOW);
      delayMicroseconds(500);
    }

    digitalWrite(sleepPin, LOW);


  }


}
